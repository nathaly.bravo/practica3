/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica3;

import java.util.Scanner;

/**
 *
 * @author Nathaly
 */
public class Memoria {

    public final static int OVERFLOW = 200000000;
    private String salida;
    private int tamanio;
    String tempOOM = "";

    public Memoria(int leng) {
        this.tamanio = leng;
        for ( int i=0;i < leng; i++) {
            try {
                this.salida += "a";
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        this.salida = tempOOM.toString();
    }

    public String getOom() {
        return salida;
    }

    public int getLength(){
        return tamanio;
    }
    
    public static void main(String[]args){
        
        //tiempo de ejecucion
        long inicio = System.currentTimeMillis();
        //memoria usada
        long beforeUsedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Memoria de inicio: " + beforeUsedMem);

        Memoria javaHeapTest = new Memoria(OVERFLOW);
        System.out.println(javaHeapTest.getOom().length());
        Scanner sc = new Scanner(System.in);
        System.out.println("Presione cualquier número");
        sc.nextInt();
        //fin memoria usada
        long afterUsedMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        System.out.println("Memoria final: " + afterUsedMem);
        //calcular memoria usada 
        long actualMemUsed = afterUsedMem - beforeUsedMem;
        System.out.println("Memoria Usada: " + actualMemUsed);
        //fin tiempo de ejecucion
        long fin = System.currentTimeMillis();
        //calcular tiempo de ejecucion
        double tiempo = (double) ((fin - inicio) / 1000);
        System.out.println(tiempo + " segundos\tUsando un OVERFLOW de: " + OVERFLOW);
    
    }
}
